package mv.data;

import java.util.ArrayList;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import saf.components.AppDataComponent;
import mv.MapViewerApp;
import mv.gui.Workspace;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    //hold data here
    //the first array holds the subregions
    //the second array holds the number of polygons per subregion
    ArrayList<Polygon>[] polygonArrayList;
    
    public DataManager(MapViewerApp initApp) {
        app = initApp;
        polygonArrayList = new ArrayList[0];
    }
    
    @Override
    public void reset() {
        Workspace space = (Workspace) app.getWorkspaceComponent();
        polygonArrayList = new ArrayList[0];
        space.getWorkspace().getChildren().clear();
    }
    
    /**
     * changes the length of the arrays
     * @param newLength - the new length of the array
     */
    public void changeLengthOfArray(int newLength){
        polygonArrayList = new ArrayList[newLength];
        for(int i=0; i<newLength; i++){
            polygonArrayList[i] = new ArrayList();
        }
    }
    /**
     * Adds a polygon to the array of arraylists
     * @param poly - the polygon to be added to the array of arraylists
     * @param position - the position of the array
     */
    public void addPolygon(Polygon poly, int position){
        poly.setFill(Color.GREEN);
        polygonArrayList[position].add(poly);
    }
    
    /**
     * Show the polygons on the screen
     */
    public void displayData(){
        Workspace space = (Workspace) app.getWorkspaceComponent();
        for(int i=0; i<polygonArrayList.length; i++){
            for(int j=0; j<polygonArrayList[i].size(); j++){
                space.getWorkspace().getChildren().add(polygonArrayList[i].get(j));
            }
        }
    }
    
    /**
     * Returns the width and height of the scene
     */
    public double[] getWidthAndHeight(){
        return(new double[]{
            app.getGUI().getWindow().getScene().getWidth(),
            app.getGUI().getWindow().getScene().getHeight()-((FlowPane)app.getGUI().getAppPane().getTop()).getHeight()});
    }
    
    public void setBackgroundToBlue(){
        Workspace space = (Workspace) app.getWorkspaceComponent();
        Polygon poly = new Polygon();
        poly.getPoints().addAll(new Double[]{
            //first point
            0.0, 0.0,
            //second point
            app.getGUI().getWindow().getScene().getWidth(), 0.0,
            //third point, accounting for the toolbar height
            app.getGUI().getWindow().getScene().getWidth(), 
            app.getGUI().getWindow().getScene().getHeight()-((FlowPane)app.getGUI().getAppPane().getTop()).getHeight(),
            //fourth point, accounting for the toolbar height
            0.0, 
            app.getGUI().getWindow().getScene().getHeight()-((FlowPane)app.getGUI().getAppPane().getTop()).getHeight()
        });
        //change in final push
        poly.setFill(Color.SKYBLUE);
        space.getWorkspace().getChildren().add(poly);
    }
}
