/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import java.io.IOException;
import java.util.ArrayList;
import static javafx.scene.input.KeyCode.G;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.transform.Scale;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    Pane linePane;
    private boolean gridLines = false;
    private int zoomedOut = 0;
    private ArrayList<Double> previousZooms;
    private int translateScale = 50;
    
    public Workspace(MapViewerApp initApp) throws IOException{
        app = initApp;
        workspace = new Pane();
        FlowPane p = (FlowPane) app.getGUI().getAppPane().getTop();
        p.getChildren().remove(0);
        p.getChildren().remove(1);
    }
    
    /**
     * Initializes line pane.
     */
    private void initializeLinePane(){
        linePane = new Pane();
        for(int i=0; i<13; i++){
            double UNIT_WIDTH = app.getGUI().getWindow().getScene().getWidth()/12;
            Line toAddLine = new Line(i*UNIT_WIDTH, 0, i*UNIT_WIDTH, app.getGUI().getWindow().getScene().getHeight()
                    -((FlowPane)app.getGUI().getAppPane().getTop()).getHeight());
            toAddLine.setStroke(Color.WHITE);
            if(i==6){
                toAddLine.setStrokeWidth(2);
            }
            else{
                toAddLine.setStrokeWidth(1);
                toAddLine.getStrokeDashArray().addAll(10d);
            }
            linePane.getChildren().add(toAddLine);
        }
        for(int i=0; i<7; i++){
            double UNIT_HEIGHT = (app.getGUI().getWindow().getScene().getHeight()
                    -((FlowPane)app.getGUI().getAppPane().getTop()).getHeight())/6;
            Line toAddLine = new Line(0, i*UNIT_HEIGHT, app.getGUI().getWindow().getScene().getWidth(), i*UNIT_HEIGHT);
            toAddLine.setStroke(Color.WHITE);
            if(i==3){
                toAddLine.setStrokeWidth(2);
            }
            else{
                toAddLine.setStrokeWidth(1);
                toAddLine.getStrokeDashArray().addAll(10d);
            }
            linePane.getChildren().add(toAddLine);
        }
    }
    /**
     * Adds listeners to workspace s.t. clicks and pressing G will
     * activate actions
     */
    private void setupHandlers(){
        Workspace work = (Workspace) app.getWorkspaceComponent();
        //the listener for mouse clicking
        work.getWorkspace().setOnMouseClicked(e -> {
            Scale scale;
            switch(e.getButton()){
                case PRIMARY:
                    translateScale /= 2;
                    scale = new Scale();
                    if(zoomedOut>=0){
                        scale.setPivotX(e.getX());
                        scale.setPivotY(e.getY());
                        previousZooms.add(e.getX());
                        previousZooms.add(e.getY());
                    }
                    else{
                        scale.setPivotX(app.getGUI().getWindow().getScene().getWidth()/2);
                        scale.setPivotY((app.getGUI().getWindow().getScene().getHeight()
                                -((FlowPane)app.getGUI().getAppPane().getTop()).getHeight())/2);
                    }
                    zoomedOut++;
                    scale.setX(2);
                    scale.setY(2);
                    work.getWorkspace().getTransforms().add(scale);
                    work.getWorkspace().toBack();
                    break;
                case SECONDARY:
                    translateScale *= 2;
                    scale = new Scale();
                    if(zoomedOut>0){
                        scale.setPivotY(previousZooms.remove(previousZooms.size()-1));
                        scale.setPivotX(previousZooms.remove(previousZooms.size()-1));
                    }
                    else{
                        //scale at the center
                        scale.setPivotX(app.getGUI().getWindow().getScene().getWidth()/2);
                        scale.setPivotY((app.getGUI().getWindow().getScene().getHeight()
                                -((FlowPane)app.getGUI().getAppPane().getTop()).getHeight())/2);
                    }
                    zoomedOut--;
                    scale.setX(.5);
                    scale.setY(.5);
                    work.getWorkspace().getTransforms().add(scale);
                    work.getWorkspace().toBack();
                    break;
                default:
                    break;
            }
        });
        //the listener for mouse pressing
        app.getGUI().getPrimaryScene().setOnKeyPressed(e -> {
            switch(e.getCode()){
                case UP:
                    work.getWorkspace().setTranslateY(work.getWorkspace().getTranslateY()+translateScale);
                    work.getWorkspace().toBack();
                    break;
                case DOWN:
                    work.getWorkspace().setTranslateY(work.getWorkspace().getTranslateY()-translateScale);
                    work.getWorkspace().toBack();
                    break;
                case LEFT:
                    work.getWorkspace().setTranslateX(work.getWorkspace().getTranslateX()+translateScale);
                    work.getWorkspace().toBack();
                    break;
                case RIGHT:
                    work.getWorkspace().setTranslateX(work.getWorkspace().getTranslateX()-translateScale);
                    work.getWorkspace().toBack();
                    break;
                case G:
                    turnOnGridLines();
                    break;
                default:
                    break;
            }
        });
    }
    
    /**
     * Turns on the grid lines
     */
    public void turnOnGridLines(){
        gridLines = !gridLines;
        if(gridLines){
            //show gridlines
            initializeLinePane();
            Workspace space = (Workspace) app.getWorkspaceComponent();
            space.getWorkspace().getChildren().add(linePane);
        }
        else{
            Workspace space = (Workspace) app.getWorkspaceComponent();
            space.getWorkspace().getChildren().remove(linePane);
        }
    }
    
    public Pane getWorkspace(){
        return workspace;
    }
    
    @Override
    public void reloadWorkspace() {
        
    }

    @Override
    public void initStyle() {
        setupHandlers();
        previousZooms = new ArrayList();
    }
}
