/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    
    static final String NUMBER_OF_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    static final String NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    static final String SUBREGIONS = "SUBREGIONS";
    static final String SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    static final String JSON_X = "X";
    static final String JSON_Y = "Y";
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        //use loadjsonfile method
        // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
                
        //load number of subregions
        //subregions array:
        //number of polygons in this region
        Polygon poly;
        JsonArray subregionArray = json.getJsonArray(SUBREGIONS);
        JsonArray subregionPolygonArray;
        JsonArray subregionPolygonArrayObjectArray;
        JsonObject subregionPolygonObject;
        JsonObject xAndYObject;
        
        try{
            dataManager.changeLengthOfArray(subregionArray.size());
            dataManager.setBackgroundToBlue();
            //the number of subregions
            for(int i=0; i<subregionArray.size(); i++){
                subregionPolygonObject = subregionArray.getJsonObject(i);
                subregionPolygonArray = subregionPolygonObject.getJsonArray(SUBREGION_POLYGONS);
                //the number of subregion polygons
                for(int j=0; j<subregionPolygonArray.size(); j++){
                    //put in values of array into the arraylist of arrays
                    //create jsonobject?
                    subregionPolygonArrayObjectArray = subregionPolygonArray.getJsonArray(j);
                    poly = new Polygon();
                    poly.setStroke(Color.BLACK);
                    poly.setStrokeWidth(.5);
                    //this iterates over the points in per subregion polygon
                    for(int k=0; k<subregionPolygonArrayObjectArray.size(); k++){
                        xAndYObject = subregionPolygonArrayObjectArray.getJsonObject(k);
                        //add the points to the polygon to evenutally return
                        addPoints(poly, xAndYObject, dataManager.getWidthAndHeight());
                    }
                    dataManager.addPolygon(poly, i);
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        dataManager.displayData();
    }
    
    public void addPoints(Polygon poly, JsonObject object, double[] widthAndHeight){
        poly.getPoints().addAll(new Double[]{
            (getDataAsDouble(object, JSON_X)+180)/360*widthAndHeight[0],
            (180-(getDataAsDouble(object, JSON_Y)+90))/180*widthAndHeight[1]
        });
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
